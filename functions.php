<?php
/**
 * storefront engine room
 *
 * @package storefront
 */

/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woothemes/theme-customisations
 */
function vl_thanh_toan($fields){
	unset($fields ['postcode']);
	$fields['state'] ['label'] ='Tỉnh';
	$fields['state']['placeholder']='chọn tỉnh';
	$fields ['city']['label'] ='Quận / Huyện';
	$fields['city']['placeholder']='Hà Đông';
	return $fields;
}
add_filter('woocommerce_default_address_fields','vl_thanh_toan');